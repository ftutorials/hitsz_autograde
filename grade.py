#!/bin/env python

import enum
from genericpath import isfile
from json.decoder import JSONDecodeError
import os
from os import link, path, replace
import json
import logging
import argparse
from posixpath import isabs
import subprocess
import shutil
from threading import Thread, Semaphore, Lock
import re
import glob
from types import GeneratorType
import zipfile
import csv
import mosspy
from bs4 import BeautifulSoup
import zlib
import pydot
import math
import pathlib
import socket
from urllib.request import urlopen
from bs4 import BeautifulSoup

logging.NOTICE = 25
logging.MILESTONE = 35

# ============================== CLASSES ==============================

class CustomFormatter(logging.Formatter):
    grey        = "\x1b[90m"
    white       = "\x1b[38m"
    bold_white  = "\x1b[97m"
    green       = "\x1b[92m"
    yellow      = "\x1b[93m"
    red         = "\x1b[91;1m"
    bold_red    = "\x1b[97;41m"
    reset       = "\x1b[0m"
    format      = "[ %(asctime)s ] [ %(levelname)s ]:\t%(message)s"

    FORMATS = {
        logging.DEBUG       : grey      + format + reset,
        logging.INFO        : white     + format + reset,
        logging.NOTICE      : bold_white+ format + reset,
        logging.WARNING     : yellow    + format + reset,
        logging.MILESTONE   : green     + format + reset,
        logging.ERROR       : red       + format + reset,
        logging.CRITICAL    : bold_red  + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


class DotDict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class FailedToCloneEnv(Exception):
    """无法下载评测环境"""


class NoEnvAvailable(Exception):
    """找不到空闲的评测环境"""


class PatternNotFound(Exception):
    def __init__(self, bad_file, bad_pattern):
        self.bad_file = bad_file
        self.bad_pattern = bad_pattern
    def __str__(self):
        return f"文件{self.bad_file}中未找到需要替换的字串{self.bad_pattern}"


class BadFileNameFormat(Exception):
    def __init__(self, value):
        self.bad_file_name = value
    def __str__(self):
        return f"文件{self.bad_file_name}的文件名格式错误"


class SrcFilesNotExist(Exception):
    def __init__(self, value):
        self.bad_file_name = value
    def __str__(self):
        return f"要拷贝的源文件或目录{self.bad_file_name}不存在"


class BadArchiveFormat(Exception):
    def __init__(self, value):
        self.bad_file_name = value
    def __str__(self):
        return f"压缩文件{self.bad_file_name}的格式错误或不支持"


class PartialSuccess(Exception):
    """解压过程中遇到问题，但解压成功"""


class MissingCriticalField(Exception):
    def __init__(self, desc):
        self.desc = desc
    def __str__(self):
        return f"缺少关键字段，{self.desc}"


class CheatEdge:
    def __init__(self, lhs, rhs, identical_line, report_url, eligible_group: int):
        (self.lhs_name, self.lhs_id, self.file) = lhs
        (self.rhs_name, self.rhs_id, self.file) = rhs
        self.similarity = identical_line
        self.identical_line = identical_line
        self.report_url = path.join("moss_reports", f"eligible{eligible_group}", report_url)


class MossClient:
    server = 'moss.stanford.edu'
    port = 7690

    def __init__(self, user_id, language, logger, thread, download_path):
        self.user_id = user_id
        self.language = language
        self.base_files = []
        self.stu_files = []
        self.logger = logger
        self.thread = thread
        self.semaphore = Semaphore(thread)
        self.download_path = download_path
        os.makedirs(self.download_path, exist_ok=True)
    
    def add_base_file(self, file_path):
        self.base_files.append(file_path)
    
    def add_stu_file(self, file_path):
        self.stu_files.append(file_path)
    
    def upload_files(self):
        logger = self.logger
        s = socket.socket()
        s.connect((self.server, self.port))
        s.send(f"moss {self.user_id}\n".encode())
        s.send(f"directory 0\n".encode())
        s.send(f"X {0}\n".encode())
        s.send(f"maxmatches {10}\n".encode())
        s.send(f"show {100}\n".encode())
        s.send(f"language {self.language}\n".encode())

        recv = s.recv(1024)
        if recv == "no":
            s.send("end\n".encode())
            s.close()
            raise Exception(f"服务器汇报不支持查重{self.language}语言。")
        
        file_id = 0

        for idx, file_path in enumerate(self.base_files):
            # base file file_id = 0
            display_name = path.basename(file_path)
            display_name = display_name.replace(" ", "_").replace("\\", "/")
            size = path.getsize(file_path)
            s.send(f"file {file_id} {self.language} {size} {display_name}\n".encode())
            with open(file_path, "rb") as to_send:
                s.send(to_send.read(size))
            logger.debug(f"模板文件{path.basename(file_path)}发送完成。({idx}/{len(self.base_files)})")

        for idx, file_path in enumerate(self.stu_files):
            file_id += 1
            display_name = path.basename(file_path)
            display_name = display_name.replace(" ", "_").replace("\\", "/")
            size = path.getsize(file_path)
            s.send(f"file {file_id} {self.language} {size} {display_name}\n".encode())
            with open(file_path, "rb") as to_send:
                s.send(to_send.read(size))
            logger.debug(f"待查重文件{path.basename(file_path)}发送完成。({idx+1}/{len(self.stu_files)})")

        s.send(f"query 0 {''}\n".encode())
        logger.debug(f"正在等待服务器回复……")
        recv = s.recv(1024).decode().replace("\n", "")
        logger.notice(f"收到回复，查重结果位于{recv}。")
        s.send("end\n".encode())
        s.close()
        return recv
    
    def download_report(self, url):
        logger = self.logger
        if len(url) == 0:
            raise Exception("下载链接为空。")
        
        links = [url]
        downloaded_links = []
        threads = []
        base_url = url + "/"

        while True:
            to_download = [u for u in links if u not in downloaded_links]
            downloaded_links = downloaded_links + to_download
            if not to_download:
                still_running = False
                for t in threads:
                    if t.is_alive():
                        still_running = True
                        break
                if still_running:
                    continue
                else:
                    break
            for link in to_download:
                self.semaphore.acquire()
                t = Thread(target=self.download_page, args=[link, self.download_path, base_url, links])
                threads.append(t)
                t.start()
        
        for t in threads:
            t.join()
        
        logger.notice(f"{url}结果下载完成。")

    
    def download_page(self, url, output_path, report_base_url, links):
        logger = self.logger
        logger.debug(f"正在下载{url}……")
        html_content = urlopen(url).read()
        logger.debug(f"{url}下载完成。")
        html_soup = BeautifulSoup(html_content, 'lxml')
        file_name = os.path.basename(url)

        if not file_name or len(file_name.split(".")) == 1: # Not file name eg. 123456789 or is None
            file_name = "index.html"
        
        for link in html_soup.find_all(['a', 'frame']):
            if link.has_attr('href'):
                link_addr = link.get('href')
            else:
                link_addr = link.get('src')
            
            link_hash = ""

            if link_addr and link_addr.find('match') != -1:
                link_fragments = link_addr.split('#')
                link_addr = link_fragments[0]
                if len(link_fragments) > 1:
                    link_hash = "#" + link_fragments[1]
                    
                basename = os.path.basename(link_addr)
                if basename == link_addr: # Handling relative urls when return discovered link list
                    link_addr = report_base_url + basename
                if link_addr not in links:
                    links.append(link_addr)

                if link.name == "a": # change url to relative
                    link['href'] = basename + link_hash
                elif link.name == "frame":
                    link['src'] = basename
        
        with open(path.join(output_path, file_name), "wb") as output:
            output.write(html_soup.encode(html_soup.original_encoding))
            logger.info(f"{url}已写入至{path.join(output_path, file_name)}")

        self.semaphore.release()
    

class Grader:
    def __init__(self, cli_configs, logger: logging.Logger):
        self.logger = logger
        logger.notice("正在初始化批量评测脚本……")

        # Load runtime config
        logger.info("正在加载运行配置……")
        self.skip_clone = cli_configs.skip_clone
        self.skip_judge = cli_configs.skip_judge
        self.skip_moss = cli_configs.skip_moss
        self.skip_moss_vis = cli_configs.skip_moss_vis
        self.skip_zip = cli_configs.skip_zip
        self.parallel_count = cli_configs.parallel
        self.base_dir = path.dirname(path.realpath(__file__))
        self.config = DotDict({})
        logger.info("运行配置加载完成。")
        
        logger.info(f"评测脚本目录地址：{self.base_dir}")
        logger.info(f"配置文件目录地址：{cli_configs.config}")
        
        # Load default config
        logger.info("正在加载默认评测配置……")
        self.fill_default_configs()
        logger.info("默认评测配置加载完成。")

        # Load config file
        logger.info("开始加载配置文件……")
        self.parse_config_file(cli_configs.config)
        logger.info(f"配置文件加载完成。")
        
        # Load cli config
        logger.info(f"正在从命令行参数覆写配置……")
        self.set_path_if_exist("student_files", cli_configs)
        self.set_path_if_exist("output_dir", cli_configs)
        self.config.output_dir = self.concat_path(self.config.output_dir)
        self.config.output_score = self.concat_path("score.csv", self.config.output_dir)
        self.config.output_bad_file = self.concat_path("bad_file.csv", self.config.output_dir)
        self.config.output_logs_dir = self.concat_path("logs", self.config.output_dir)
        self.config.output_moss_reports = self.concat_path("moss_reports", self.config.output_dir)
        self.config.output_moss_visualize = self.concat_path("visualize.svg", self.config.output_dir)
        self.set_path_if_exist("cache_dir", cli_configs)
        self.config.cache_dir = self.concat_path(self.config.cache_dir)
        self.config.clean_repo = self.concat_path("clean_repo", self.config.cache_dir)
        self.config.env_root = self.concat_path("env_root", self.config.cache_dir)
        self.config.moss_files = self.concat_path("moss_files", self.config.cache_dir)
        self.set_if_exist("codex", cli_configs)
        logger.info(f"覆写配置完成。")

        logger.notice(f"配置初始化完成。")
        self.explain_config(logging.INFO)

        self.semaphore = Semaphore(self.parallel_count)
        self.env_available = [Lock() for _ in range(0, self.parallel_count)]
        self.result_mutex = Lock()
        self.output_mutex = Lock()
        self.results = []
        self.bad_files = []
        self.report_urls = {}
        
        logger.milestone(f"批量评测脚本初始化完成。")

    
    def batch_grade(self):
        logger = self.logger

        if self.skip_judge:
            logger.warning(f"检测到--skip-judge，跳过批量评测阶段。")
            return
        
        if not path.exists(self.config.student_files) or not os.listdir(self.config.student_files):
            logger.warning(f"检测到学生提交文件夹为不存在或为空。跳过批量评测阶段。")
            return
        
        logger.notice(f"开始构造批量评测环境……")
        try:
            self.clone_repo()
        except Exception as e:
            logger.fatal(f"无法下载评测环境，因为'{e}'。")
            exit(0)
        logger.milestone(f"开始执行批量评测……")
        threads = []
        student_files = [path.join(self.config.student_files, f) for f in os.listdir(self.config.student_files) if path.isfile(path.join(self.config.student_files, f))]
        for f in student_files:
            self.semaphore.acquire()
            logger.debug(f"检测到提交文件{f}，准备评测。")
            t = Thread(target=self.single_grade, args=(f, ))
            threads.append(t)
            t.start()
        for t in threads:
            t.join()
        logger.milestone(f"评测已全部完成。")
        logger.info(f"开始清理评测环境。")
        clean_thread = []
        for env_folder in [path.join(self.config.env_root, f) for f in os.listdir(self.config.env_root) if f.startswith("env")]:
            logger.debug(f"清理{env_folder}。")
            t = Thread(target=shutil.rmtree, args=(env_folder, ))
            clean_thread.append(t)
            t.start()
        for t in clean_thread:
            t.join()
        logger.info(f"评测环境清理完成。")
        logger.info(f"开始保存得分与评测记录。")
        os.makedirs(path.dirname(self.config.output_score), exist_ok=True)
        try:
            with open(self.config.score_template, "r", encoding=self.config.codex) as template_file:
                with open(self.config.output_score, "w", encoding=self.config.codex) as score_file:
                    writer = csv.writer(score_file)
                    writer.writerow(["学号", "姓名", "得分", "注释"])
                    for tem_row in csv.reader(template_file):
                        logger.debug(f"正在填写{tem_row[1]}（{tem_row[0]}）的成绩...")
                        found = False
                        for row in self.results:
                            if row[0] == tem_row[0]:
                                if row[1] != tem_row[1]:
                                    logger.warning(f"发现不一致：提交文件中学号{row[0]}对应{row[1]}，但模板文件中对应{tem_row[1]}。")
                                    row[3] = row[3] + f";模板文件中姓名{tem_row[1]}与提交姓名{row[1]}不一致"
                                writer.writerow(row)
                                found = True
                                break
                        if not found:
                            logger.warning(f"未发现{tem_row[1]}（{tem_row[0]}）的提交文件。")
                            writer.writerow((tem_row[0], tem_row[1], 0, "未找到提交文件"))
                                
            logger.notice(f"成绩已保存至{self.config.output_score}。")
            with open(self.config.output_bad_file, "w", encoding=self.config.codex) as bad_list:
                writer = csv.writer(bad_list)
                for bad in self.bad_files:
                    writer.writerow(bad)
            logger.notice(f"异常列表已保存至{self.config.output_bad_file}。")
        except Exception as e:
            logger.fatal(f"未能成功保存评测结果。错误信息如下：{e}")
            exit(0)
        logger.milestone(f"评测结果已保存。")


    def plagiarism_test(self):
        logger = self.logger
        if self.skip_moss:
            logger.warning(f"检测到--skip-moss，跳过代码查重阶段。")
            return
            
        if not path.exists(self.config.moss_files) or not os.listdir(self.config.moss_files):
            logger.warning(f"检测到moss提交文件夹为不存在或为空。跳过代码查重阶段。")
            return

        
        for idx, eligible in enumerate(self.config.moss_config.eligibles):
            desc_str = eligible.desc if eligible.desc else eligible.path_regex
            logger.debug(f"开始对{desc_str}执行代码查重。")
            count = 0
            moss_client = MossClient(self.config.moss_config.userid, eligible.language, self.logger, self.parallel_count, path.join(self.config.output_moss_reports, f"eligible{idx}"))
            for root, _, files in os.walk(self.config.moss_config.templates):
                for file in files:
                    moss_client.add_base_file(path.join(root, file))
            
            stu_files = [path.join(self.config.moss_files, f"eligible{idx}", f) for f in os.listdir(path.join(self.config.moss_files, f"eligible{idx}"))]
            if not stu_files:
                logger.warning(f"对于{desc_str}，没有可以检查的文件。")
            for stu in stu_files:
                moss_client.add_stu_file(stu)
            
            report_url = moss_client.upload_files()

            if not report_url:
                logger.error(f"{desc_str}的代码查重报告发送失败。请检查网络连接。")
                exit(0)

            logger.notice(f"{desc_str}的代码查重报告已生成，报告URL为{report_url}。开始下载到本地。")
            self.report_urls[desc_str] = report_url
            moss_client.download_report(report_url)
            logger.milestone(f"{desc_str}的代码查重报告已下载到本地。")
        
        logger.milestone(f"MOSS的代码查重报告已全部生成并下载到本地{self.config.output_moss_reports}处。")
    

    def visualize_plagiarism(self):
        logger = self.logger
        if self.skip_moss_vis:
            logger.warning(f"检测到--skip-moss-vis，跳过查重结果可视化生成阶段。")
            return
        
        if not path.exists(self.config.output_moss_reports) or not os.listdir(self.config.output_moss_reports):
            logger.warning(f"检测到moss检测结果文件夹为不存在或为空。跳过可视化生成阶段。")
            return
        
        logger.notice("正在生成查重可视化结果……")
        edges = []
        for idx, eligible in enumerate(self.config.moss_config.eligibles):
            desc_str = eligible.desc if eligible.desc else eligible.path_regex
            logger.info(f"开始读取{desc_str}的查重报告。")
            index_html = path.join(self.config.output_moss_reports, f"eligible{idx}", "index.html")
            edges = edges + self.parse_report(index_html, eligible_group=idx)
        logger.notice(f"开始过滤边，当前阈值为{self.config.moss_config.threshold}雷同行。")
        edges = [e for e in edges if e.similarity >= self.config.moss_config.threshold]
        logger.notice(f"边过滤完成。")
        if self.config.anonymous:
            logger.warning(f"发现--anonymous，开始进行匿名替换。")
            for edge in edges:
                edge.lhs_name = self.gen_hash(edge.lhs_name)
                edge.lhs_id = self.gen_hash(edge.lhs_id)
                edge.rhs_name = self.gen_hash(edge.rhs_name)
                edge.rhs_id = self.gen_hash(edge.rhs_id)
                logger.debug(f"关于{edge.file}的边：{edge.lhs_name}（{edge.lhs_id}）<== {edge.similarity}% ==> {edge.rhs_name}（{edge.rhs_id})，报告URL位于{edge.report_url}。")
            logger.info(f"匿名替换完成。")
        logger.notice(f"开始构造图。")
        graph = pydot.Dot(graph_type='graph')
        max_sim = max([e.similarity for e in edges])
        for e in edges:
            ratio = self.normalize(e.similarity, max_sim, self.config.moss_config.threshold)
            extra_opts = {
                'color': self.gen_color(ratio),
                'fontcolor': self.gen_color(ratio),
                'penwidth': int(1+ratio*3),
                'label': f"相同行数：{e.identical_line}\n雷同文件：{e.file}",
                'labelURL': e.report_url,
                'URL': e.report_url
            }
            graph.add_edge(pydot.Edge(f"{e.lhs_name}（{e.lhs_id}）", f"{e.rhs_name}（{e.rhs_id}）", **extra_opts))
        logger.notice(f"图构造成功。")
        if path.exists(self.config.output_moss_visualize):
            os.remove(self.config.output_moss_visualize)
        graph.write(self.config.output_moss_visualize, format='svg')
        logger.milestone(f"完成可视化图像输出，输出位于{self.config.output_moss_visualize}。")


    def pack_result(self):
        if self.skip_zip:
            self.logger.warning(f"检测到--skip-zip，跳过结果压缩包生成。")
            return
        if not path.exists(self.config.output_dir) or not os.listdir(self.config.output_dir):
            self.logger.warning(f"结果文件夹为空，跳过结果压缩包生成。")
            return
        self.logger.notice(f"正在打包评测结果……")
        shutil.make_archive(path.join(self.base_dir, "result"), "zip", self.config.output_dir, logger=self.logger)
        self.logger.milestone(f"所有结果已打包至{path.join(self.base_dir, 'result.zip')}。")


    def clone_repo(self):
        logger = self.logger
        if path.exists(self.config.clean_repo) and os.listdir(self.config.clean_repo) and self.skip_clone:
            logger.warning(f"检测到--skip-clone标签，且缓存合法，跳过评测环境下载。")
            return
        if self.skip_clone:
            logger.warning(f"检测到--skip-clone标签，但是缓存不合法，正在重新下载。")
        logger.debug(f"正在清空缓存文件夹……")
        self.delete_stuff(self.config.clean_repo)
        os.makedirs(self.config.clean_repo)
        logger.info(f"开始下载……")
        try:
            subprocess.check_output(["git", "clone", "--recursive", "-b", self.config.test_env.branch, self.config.test_env.repository, self.config.clean_repo])
        except subprocess.CalledProcessError:
            raise FailedToCloneEnv
        logger.info(f"评测环境已下载至{self.config.clean_repo}。")
    

    def normalize(self, val, upper, lower):
        if val > upper or val < lower:
            self.logger.fatal(f"无法正则化：{val}不在[{lower}, {upper}]区间。")
            raise ValueError
        return (val-lower)/(upper-lower)

    
    def gen_color(self, ratio):
        color_list = [
            "#ad7d00",
            "#bf7100",
            "#d46100",
            "#e94700",
            "#ff0000",
        ]
        return color_list[4 if ratio == 1 else math.floor(ratio*5)]
        

    def gen_hash(self, input: str):
        return (hex(zlib.crc32(input.encode("utf-8")))[2:]).upper()
    

    def parse_line(self, td_content: str):
        file, similarity = td_content.split()
        stu_name, stu_id, file = self.parse_name(file)
        return (stu_id, stu_name, file)


    def parse_report(self, file: str, eligible_group: int):
        logger = self.logger
        with open(file, 'r') as f:
            soup = BeautifulSoup(f.read(), 'html.parser')
        matches = []
        # Extract all rows except first (title)
        for tb_row in soup.table('tr')[1:]:
            lhs, rhs, identical_line = map(lambda x: x.text, tb_row('td'))
            lhs_info = self.parse_line(lhs)
            rhs_info = self.parse_line(rhs)
            identical_line = int(re.search(r'\d+', identical_line).group())
            report_url = tb_row.a['href']
            # Only report if is from different students
            if lhs_info[1] != rhs_info[1]:
                edge = CheatEdge(lhs_info, rhs_info, identical_line, report_url, eligible_group)
                matches.append(edge)
                logger.debug(f"发现关于{edge.file}的边：{edge.lhs_name}（{edge.lhs_id}）<== {edge.similarity}% ==> {edge.rhs_name}（{edge.rhs_id})，报告URL位于{edge.report_url}。")
        return matches


    def single_grade(self, student_zip_path):
        logger = self.logger
        process_msg = []
        missing_files = []
        bad_override = []

        logger.debug("开始初始化评测环境……")

        try:
            stu_id, stu_name, _ = self.parse_name(student_zip_path)
            env_id = self.alloc_env()
        except Exception as e:
            logger.error(f"评测环境初始化失败，因为{e}。")
            process_msg.append(f"评测环境初始化失败，因为{e}")
            self.save_bad_file(student_zip_path, process_msg)
            self.semaphore.release()
            exit(0)
        
        env_path = path.join(self.config.env_root, f"env{env_id}")
        env_judge_path = path.join(env_path, "judge")
        env_stu_path = path.join(env_path, "stu")

        logger.debug(f"正在为{stu_name}（{stu_id}）清理环境……")
        self.delete_stuff(env_path)
        os.makedirs(env_path, exist_ok=True)
        logger.debug(f"已经为{stu_name}（{stu_id}）完成环境清理。")

        logger.debug(f"正在将{student_zip_path}解压至{env_stu_path}……")
        try:
            self.extract_nested(student_zip_path, env_stu_path)
        except PartialSuccess as e:
            logger.info(f"{stu_name}（{stu_id}）的提交文件在解压过程中遇到问题，但仍然解压成功。")
            process_msg.append(f"提交文件在解压过程中遇到问题，但仍然解压成功。")
        except Exception as e:
            logger.info(f"{stu_name}（{stu_id}）的提交文件无法解压，因为'{e}'。")
            process_msg.append(f"提交文件因为'{e}'而无法解压")
            self.save_bad_file(student_zip_path, process_msg)
            self.save_result_and_exit(env_id, stu_name, stu_id, 0, process_msg)
        logger.debug(f"已经将{student_zip_path}解压至{env_stu_path}。")
        
        logger.debug(f"正在将环境{self.config.clean_repo}拷贝至{env_judge_path}……")
        try:
            self.delete_stuff(env_judge_path)
            os.makedirs(env_judge_path, exist_ok=True)
            shutil.copytree(self.config.clean_repo, path.join(env_judge_path), dirs_exist_ok=True)
        except Exception as e:
            logger.info(f"评测{stu_name}（{stu_id}）时无法拷贝评测环境，因为'{e}'。")
            process_msg.append(f"因为'{e}'，无法拷贝评测环境")
            self.save_result_and_exit(env_id, stu_name, stu_id, 0, process_msg)
        logger.debug(f"已经将环境{self.config.clean_repo}拷贝至{env_judge_path}。")

        # 暂时先不查重
        # logger.info(f"开始备份{stu_name}（{stu_id}）的MOSS查重文件……")
        # for idx, eligible in enumerate(self.config.moss_config.eligibles):
        #     try:
        #         self.safe_copy(eligible.path_regex, env_stu_path, path.join(self.config.moss_files, f"eligible{idx}", f"{stu_id}_{stu_name}_{{file_name}}"), exempt=eligible.exempt)
        #     except Exception as e:
        #         logger.info(f"评测{stu_name}（{stu_id}）时, 未找到需要查重的文件({eligible.desc if eligible.desc else eligible.path_regex})。")
        #         process_msg.append(f"未找到需要查重的文件（{eligible.desc if eligible.desc else eligible.path_regex}）。")
        #         missing_files.append(eligible.desc if eligible.desc else eligible.path_regex)
        # logger.info(f"{stu_name}（{stu_id}）的MOSS查重文件备份完成。")
        
        logger.info(f"开始保存{stu_name}（{stu_id}）的解答至输出……")
        for f in self.config.file_list:
            if f.copy_to_output:
                try:
                    self.safe_copy(f.src, env_stu_path, self.concat_path(f.copy_to_output, self.config.output_dir), exempt=f.exempt, optional = f.optional)
                except Exception as e:
                    logger.info(f"评测{stu_name}（{stu_id}）时, 未找到需要保存的文件'{f.desc if f.desc else f.src}'，因为{e}。")
                    process_msg.append(f"未找到需要保存的文件‘{f.desc if f.desc else f.src}’（{e}）。")
                    missing_files.append(f.desc if f.desc else f.src)
        logger.info(f"{stu_name}（{stu_id}）的解答保存完成。")

        logger.info(f"开始替换代入{stu_name}（{stu_id}）的解答……")
        for f in self.config.file_list:
            if f.dst:
                try:
                    self.safe_copy(f.src, env_stu_path, self.concat_path(f.dst, env_judge_path), exempt=f.exempt, optional = f.optional)
                except Exception as e:
                    logger.info(f"评测{stu_name}（{stu_id}）时, 未找到需要代入的文件'{f.desc if f.desc else f.src}'，因为{e}。")
                    process_msg.append(f"未找到需要代入的文件‘{f.desc if f.desc else f.src}’（{e}）。")
                    missing_files.append(f.desc if f.desc else f.src)
        logger.info(f"{stu_name}（{stu_id}）的解答代入完成。")
        
        logger.info(f"正在根据配置最终覆写{stu_name}（{stu_id}）的评测环境……")
        overrides = self.config.test_env.overrides
        for to_override, o_conf in overrides.items():
            if o_conf.type == "alteration":
                try:
                    original_expanded = o_conf.original.format(env_id=env_id, stu_id=stu_id, stu_name=stu_name)
                    altered_expanded = o_conf.altered.format(env_id=env_id, stu_id=stu_id, stu_name=stu_name)
                    self.alternate_file(env_judge_path, to_override, original_expanded, altered_expanded)
                    logger.debug(f"完成对{to_override}内容的替换。")
                except Exception as e:
                    logger.error(f"在执行{stu_name}（{stu_id}）的环境构建时出现错误：{e}。")
                    process_msg.append(f"替换{to_override}内容时出错：{e}")
                    bad_override.append(to_override)
            elif o_conf.type == "creation":
                try:
                    content_expanded = o_conf.content.format(env_id=env_id, stu_id=stu_id, stu_name=stu_name)
                    self.create_file(env_judge_path, to_override, content_expanded)
                    logger.debug(f"完成对{to_override}内容的替换。")
                except Exception as e:
                    logger.error(f"在执行{stu_name}（{stu_id}）的环境构建时出现错误：{e}。")
                    process_msg.append(f"生成{to_override}时出错：{e}")
                    bad_override.append(to_override)
        logger.info(f"{stu_name}（{stu_id}）的评测环境最终覆写完成。")
        
        if missing_files:
            logger.info(f"在{stu_name}（{stu_id}）的提交中，缺少下列文件：{', '.join(missing_files)}。")
            process_msg.append(f"提交中缺少下列文件: {', '.join(missing_files)}")

        logger.notice(f"{stu_name}（{stu_id}）的评测环境构造完成。")

        output_stdout = []
        output_stderr = []
        found = False
        second_found = True
        score = "0"
        for cmd in self.config.test_env.test_script:
            replaced_cmd = [c.format(env_id=env_id, stu_id=stu_id, stu_name=stu_name) for c in cmd]
            logger.debug(f"{stu_name}（{stu_id}）的评测环境开始执行指令{' '.join(replaced_cmd)}")
            p = subprocess.run(replaced_cmd, cwd=env_judge_path, capture_output=True)

            self.output_mutex.acquire()
            logger.debug(f"{stu_name}（{stu_id}）的评测环境指令{' '.join(replaced_cmd)}执行完成，输出如下：")
            logger.debug(f"================================ stdout ================================")
            for line in [line.decode('utf-8') for line in p.stdout.split(b'\n')]:
                logger.debug(line)
            logger.debug(f"================================ stderr ================================")
            for line in [line.decode('utf-8') for line in p.stderr.split(b'\n')]:
                logger.debug(line)
            logger.debug(f"========================================================================")
            self.output_mutex.release()

            score_out = [line.decode('utf-8') for line in p.stdout.split(b'\n')]
            for line in score_out:
                score_match_res = re.match(self.config.test_env.result_regex, line)
                if score_match_res:
                    if found and second_found:
                        logger.warning(f"在运行{stu_name}（{stu_id}）的评测时，检测到多个符合result_regex结果匹配正则表达式的输出。")
                        process_msg.append(f"检测到多个符合result_regex结果匹配正则表达式的输出。")
                        second_found = False
                    else:
                        score = score_match_res.groups()[0]
                        found = True
                        # not breaking, using last match as result if multiple matches appeared.
            output_stdout.append(p.stdout.decode("utf-8"))
            output_stderr.append(p.stderr.decode("utf-8"))
            if p.returncode != 0:
                logger.info(f"评测{stu_name}（{stu_id}）时执行命令“{' '.join(replaced_cmd)}”出现问题，返回值{p.returncode}。")
                # process_msg.append(f"命令“{' '.join(replaced_cmd)}”未成功执行")
        logger.info(f"{stu_name}（{stu_id}）的评测已完成。")

        logger.info(f"正在将{stu_name}（{stu_id}）的输出保存至文件……")
        log_contents = zip([" ".join(replaced_cmd) for replaced_cmd in self.config.test_env.test_script], output_stdout, output_stderr)
        log_literial = ""
        for single_line, sout, serr in log_contents:
            log_literial = log_literial + f"\n命令{single_line}的执行结果如下：\n"
            log_literial = log_literial + f"================================ stdout ================================\n"
            log_literial = log_literial + sout
            log_literial = log_literial + f"================================ stderr ================================\n"
            log_literial = log_literial + serr
            log_literial = log_literial + f"========================================================================\n"
        self.create_file(self.config.output_logs_dir, f"{stu_id}_{stu_name}_scriptoutput.log", log_literial)
        logger.info(f"{stu_name}（{stu_id}）的评测过程输出已保存至文件{self.config.output_logs_dir}/{stu_id}_{stu_name}_scriptoutput.log。")
        if missing_files or bad_override:
            self.save_bad_file(student_zip_path, process_msg)
        self.save_result_and_exit(env_id, stu_name, stu_id, score, process_msg)
        

    def fill_default_configs(self):
        self.config.codex = "GB18030"
        self.config.plagiarism_threshold = 90
        self.config.anonymous = False
        self.config.output_dir = self.concat_path("result")
        self.config.student_files = self.concat_path("student_files")
        self.config.cache_dir = self.concat_path("cache")


    def parse_config_file(self, file_position: str):
        logger = self.logger
        logger.info(f"正在加载配置文件……")
        config_dir = path.dirname(path.realpath(file_position))
        with open(file_position, "r", encoding=self.config.codex) as config_file:
            config_dict = DotDict(json.loads(config_file.read()))

        # Convert file to DotDict
        file_list = []
        for file in config_dict.file_list:
            file_config = DotDict(file)
            if "src" not in file_config:
                raise MissingCriticalField(f"file_list中每项都必须有一个\"src\"字段，以指示其在学生所提交文件中的位置。")
            file_list.append(file_config)
        config_dict.file_list = file_list

        # Load moss config
        if config_dict.moss_config:
            config_dict.moss_config = DotDict(config_dict.moss_config)
            if "userid" not in config_dict.moss_config:
                raise MissingCriticalField(f"缺少Moss查重系统的用户id（moss_config::userid字段）。")
            if not config_dict.moss_config.eligibles:
                logger.warning("缺少moss_config::eligibles字段，将没有文件被查重。")
            else:
                eligibles = []
                for eligible in config_dict.moss_config.eligibles:
                    dotdict_eligible = DotDict(eligible)
                    if "path_regex" not in dotdict_eligible:
                        raise MissingCriticalField(f"eligibles中有项缺少path_regex字段。")
                    if "language" not in dotdict_eligible:
                        raise MissingCriticalField(f"eligibles中有项缺少language字段。")
                    eligibles.append(dotdict_eligible)
                config_dict.moss_config.eligibles = eligibles
            if not config_dict.moss_config.templates:
                logger.warning("缺少moss_config::templates字段，可能会误报代码重复。")
            else:
                config_dict.moss_config.templates = self.concat_path(config_dict.moss_config.templates, config_dir)
            if not config_dict.moss_config.threshold:
                logger.debug("缺少moss_config::threshold，使用默认值90。")
                config_dict.moss_config.threshold = 90
        
        # Load test_env config
        if not config_dict.test_env:
            raise MissingCriticalField("缺少test_env字段。")
        config_dict.test_env = DotDict(config_dict.test_env)
        for o, conf in config_dict.test_env.overrides.items():
            conf = DotDict(conf)
            config_dict.test_env.overrides[o] = conf
            if conf.type != "alteration" and conf.type != "creation":
                logger.fatal(f"在配置文件中检测到不支持该评测环境文件{o}的覆盖操作子类型'{conf.type}'。")
                exit(0)

        config_dict.score_template = self.concat_path(config_dict.score_template, config_dir)
        if config_dict.output_dir:
            config_dict.output_dir = self.concat_path(config_dict.output_dir)
        if config_dict.student_files:
            config_dict.student_files = self.concat_path(config_dict.student_files)
        if config_dict.cache_dir:
            config_dict.cache_dir = self.concat_path(config_dict.cache_dir)
        
        # Load all config into self.config
        for name, conf in config_dict.items():
            if name in self.config:
                logger.warning(f"配置文件覆写了配置条目{name}，旧值为{self.config.name}，新值为{conf}")
            self.config[name] = conf


    def explain_config(self, log_level: int):
        logger = self.logger

        logger.log(log_level, "")
        logger.log(log_level, "==================== 通用评测配置 ====================")
        logger.log(log_level, "")
        
        logger.log(log_level, f"并行评测数量: {self.parallel_count}")
        logger.log(log_level, f"待测文件位置: {self.config.student_files}")
        if self.config.student_files:
            logger.log(log_level, f"待测文件位置: {self.config.student_files}")
        if self.config.output_dir:
            logger.log(log_level, f"输出目录: {self.config.output_dir}")
        if self.config.cache_dir:
            logger.log(log_level, f"缓存目录: {self.config.cache_dir}")

        logger.log(log_level, "")
        logger.log(log_level, "==================== 提交文件配置 ====================")
        logger.log(log_level, "")

        for idx, conf in enumerate(self.config.file_list):
            logger.log(log_level, f"条目{idx}：")
            logger.log(log_level, f"\t源文件位置：{conf.src}")
            if conf.dst:
                chn_str = conf.dst
                logger.log(log_level, f"\t拷贝至评测环境/{chn_str}处。")
            if conf.copy_to_output:
                chn_str = conf.copy_to_output
                logger.log(log_level, f"\t拷贝至输出文件夹/{chn_str}处。")


        logger.log(log_level, "")
        logger.log(log_level, "==================== 代码查重配置 ====================")
        logger.log(log_level, "")

        moss_conf = self.config.moss_config
        if moss_conf.userid:
            logger.log(log_level, f"MOSS用户id: {moss_conf.userid}")
        if moss_conf.threshold:
            logger.log(log_level, f"查重报告阈值: {self.config.threshold}")
        logger.log(log_level, f"生成匿名查重结果: {'是' if moss_conf.anonymous else '否'}")
        
        for idx, reg in enumerate(moss_conf.eligibles):
            logger.log(log_level, f"查重文件群{idx}")
            logger.log(log_level, f"\t路径正则串：{reg.path_regex}")
            logger.log(log_level, f"\t语言：{reg.language}")
        
        logger.log(log_level, "")
        logger.log(log_level, "==================== 评测环境配置 ====================")
        logger.log(log_level, "")

        env_conf = self.config.test_env
        logger.log(log_level, f"评测仓库地址: {env_conf.repository}")
        logger.log(log_level, f"评测仓库分支: {env_conf.branch}")
        logger.log(log_level, f"评测命令: ")
        for cmd in env_conf.test_script:
            logger.log(log_level, "\t" + " ".join(cmd).format(stu_name="{学生姓名}", stu_id="{学生id}", env_id="{评测环境编号}"))
        if env_conf.overrides:
            logger.log(log_level, f"覆写评测环境：")
            for override, o_conf in self.config.test_env.overrides.items():
                if o_conf.type == "alteration":
                    logger.log(log_level, f"\t替换文件{override}的内容：")
                    logger.log(log_level, f"\t\t原串：{o_conf.original}")
                    logger.log(log_level, f"\t\t新串：{o_conf.altered}")
                elif o_conf.type == "creation":
                    logger.log(log_level, f"\t创建文件{override}：")
                    logger.log(log_level, f"\t\t内容：{o_conf.content}")


    def set_if_exist(self, name, cli_configs):
        if vars(cli_configs)[name]:
            self.config[name] = vars(cli_configs)[name]
    

    def set_path_if_exist(self, name, cli_configs):
        if vars(cli_configs)[name]:
            self.config[name] = self.concat_path(vars(cli_configs)[name])
    
    
    def concat_path(self, append, base=None):
        if not base:
            base = self.base_dir
        return append if path.isabs(append) else path.join(base, append)

    
    def alternate_file(self, env_root, file_path, original, altered):
        self.logger.debug(f"正在{env_root}中更改文件{file_path}的内容。")
        to_override = path.join(env_root, file_path)
        with open(to_override, "r") as rf:
            replaced_txt = rf.read()
        if not replaced_txt.find(original):
            raise PatternNotFound(file_path, original)
        replaced_txt = replaced_txt.replace(original, altered)
        with open(to_override, "w", encoding='utf-8') as wf:
            wf.write(replaced_txt)
    

    def create_file(self, env_root, file_path, content):
        self.logger.debug(f"正在在{env_root}中创建文件{file_path}。")
        to_create = path.join(env_root, file_path)
        os.makedirs(path.dirname(to_create), exist_ok=True)
        with open(to_create, "w") as wf:
            wf.write(content)


    def safe_copy(self, src: str, src_root: str, dst: str, exempt=None, optional=False):
        logging.debug(f"将{src_root}中的{src}拷贝至{dst}。")
        if not path.isabs(src_root):
            src_root = self.concat_path(src_root)
        if not path.exists(src_root) or not path.isdir(src_root):
            raise SrcFilesNotExist(src_root)
        
        not_found = True
        for root, _, files in os.walk(src_root):
            for name in files:
                full_path = path.join(root, name)
                # logging.debug(f"检查{full_path}是否满足{src}。")
                match_res = re.match(src, full_path)
                exempt_res = None
                if exempt:
                    exempt_res = re.match(exempt, full_path)
                if match_res and not exempt_res:
                    dst_path = dst.format(groups = match_res.groups(), file_name = path.basename(full_path))
                    if not path.exists(full_path):
                        raise SrcFilesNotExist(full_path)
                    logging.debug(f"找到文件{full_path}，其将被拷贝至{dst_path}。")
                    not_found = False
                    self.delete_stuff(dst_path)
                    os.makedirs(path.dirname(dst_path), exist_ok=True)
                    if path.isfile(full_path):
                        shutil.copy(full_path, dst_path)
                    else:
                        shutil.copytree(full_path, dst_path)
        if not_found and not optional:
            raise SrcFilesNotExist(src)


    def delete_stuff(self, path_str):
        if path.exists(path_str):
            if path.isfile(path_str):
                os.remove(path_str)
            else:
                shutil.rmtree(path_str, ignore_errors=True)


    def alloc_env(self):
        for i, lock in enumerate(self.env_available):
            if lock.acquire(blocking=False):
                return i
        raise NoEnvAvailable


    def free_env(self, env_id):
        self.env_available[env_id].release()


    def save_result_and_exit(self, env_id, stu_name, stu_id, score, process_msg):
        logger = self.logger
        self.output_mutex.acquire()
        logger.notice(f"对{stu_name}（{stu_id}）的评测已执行完成，得分为{score}。")
        if process_msg:
            logger.warning(f"在上述评测执行过程中曾出现问题：")
            for m in process_msg:
                logger.warning(f"\t{m}。")
        self.output_mutex.release()
        self.result_mutex.acquire()
        self.results.append([stu_id, stu_name, score, ";".join(process_msg) if process_msg else "正常"])
        self.result_mutex.release()
        self.free_env(env_id)
        self.semaphore.release()
        exit(0)


    def save_bad_file(self, file_name, process_msg):
        logger = self.logger
        self.output_mutex.acquire()
        logger.info(f"文件'{path.basename(file_name)}'不符合要求，因为'{ '；'.join(process_msg)}'。")
        self.output_mutex.release()
        self.result_mutex.acquire()
        self.bad_files.append((path.basename(file_name), "；".join(process_msg)))
        self.result_mutex.release()


    def parse_name(self, file_name):
        if path.isabs(file_name):
            file_name = path.basename(file_name)
        parse_regex = r"^([\w\u4e00-\u9fa5\u2000-\u206F]{4,12})_([\w\u4e00-\u9fa5\u2000-\u206F]{2,30})_(.*)$"
        match_res = re.match(parse_regex, path.basename(file_name))
        if not match_res:
            raise BadFileNameFormat(path.basename(file_name))
        return match_res.groups()


    def extract_nested(self, archive_path, dest_path):
        self.extract(archive_path, dest_path)
        archieves = glob.glob(dest_path + "/**/*.zip", recursive=True) + glob.glob(dest_path + "/**/*.rar", recursive=True)
        while archieves:
            self.logger.info(f"在提交文件{path.basename(archive_path)}中发现嵌套压缩包{[path.basename(f) for f in archieves]}。正在重新解压。")
            for cf in archieves:
                self.extract(cf, dest_path)
                os.remove(cf)   # avoid infinite recursion
            archieves = glob.glob(dest_path + "/**/*.zip", recursive=True) + glob.glob(dest_path + "/**/*.rar", recursive=True)
        self.set_rw_permission(dest_path)


    def extract(self, archive_path, dest_path):
        try:
            os.makedirs(path.dirname(path.dirname(dest_path)), exist_ok=True)
            if archive_path.endswith(".zip"):
                p = subprocess.run(["unzip", "-q", "-O", "cp936", "-d", dest_path, archive_path], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                if p.returncode == 0:
                    return
                elif p.returncode in [1, 2]:
                    raise PartialSuccess
            elif archive_path.endswith(".rar"):
                subprocess.check_call(["unrar", "x", archive_path, dest_path], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                return
            else:
                raise BadArchiveFormat(path.basename(archive_path))
        except Exception as e:
            raise BadArchiveFormat(path.basename(archive_path)) from e
    

    def set_rw_permission(self, to_set):
        _ = subprocess.run(["chmod", "-R", "777", to_set], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def main():
    logging.NOTICE = 25
    logging.MILESTONE = 35
    arg_parser = argparse.ArgumentParser(description="通用自动评测脚本。脚本执行参数会覆盖配置文件参数。")
    arg_parser.add_argument("config", type=str, help="自动评测脚本配置文件。")
    arg_parser.add_argument("--parallel", "-p", type=int, default=1, help="并行任务数量。默认为1。")
    arg_parser.add_argument('--verbose', '-v', action='count', default=0, help="输出等级。v越多，输出越多，最多四个。")
    arg_parser.add_argument("--codex", "-c", type=str, default=None, help="输出.csv文件的编码。默认为GB18030。")
    arg_parser.add_argument("--moss-userid", type=str, default=None, help="MOSS评测系统的用户ID。")
    arg_parser.add_argument("--plagiarism-threshold", "-t", type=int, default=None, help="抄袭判定阈值。默认为90。")
    arg_parser.add_argument("--anonymous", "-a", action="store_true", default=False, help="将生成的可视化结果匿名化。")
    arg_parser.add_argument("--skip-clone", action="store_true", default=False, help="跳过下载评测环境，使用缓存内容。")
    arg_parser.add_argument("--skip-judge", action="store_true", default=False, help="跳过评测，使用缓存内容进行查重。")
    arg_parser.add_argument("--skip-moss", action="store_true", default=False, help="跳过代码查重。")
    arg_parser.add_argument("--skip-moss-vis", action="store_true", default=False, help="跳过代码查重可视化生成。")
    arg_parser.add_argument("--skip-zip", action="store_true", default=False, help="跳过结果压缩包生成。")
    arg_parser.add_argument("--repository", "-r", type=str, default=None, help="评测环境Repo。支持本地文件与远程git链接。")
    arg_parser.add_argument("--branch", "-b", type=str, default=None, help="评测环境所在的Git分支。")
    arg_parser.add_argument("--student-files", "-f", type=str, default=None, help="学生文件压缩包所在的文件夹。默认位于./student_files。")
    arg_parser.add_argument("--output-dir", "-o", type=str, default=None, help="默认输出文件夹，内部包含score.csv和moss_report。默认为./result。")
    arg_parser.add_argument("--cache-dir", type=str, default=None, help="缓存文件夹。默认位于./cache。")

    args = arg_parser.parse_args()
    
    ch = logging.StreamHandler()
    logger = logging.getLogger()
    
    logging.addLevelName(logging.DEBUG      , "调试")
    logging.addLevelName(logging.INFO       , "信息")
    logging.addLevelName(logging.NOTICE     , "通知")
    logging.addLevelName(logging.WARNING    , "警告")
    logging.addLevelName(logging.MILESTONE  , "重点")
    logging.addLevelName(logging.ERROR      , "错误")
    logging.addLevelName(logging.FATAL      , "致命")

    logging_level = logging.DEBUG
    if args.verbose == 0:
        logging_level = logging.MILESTONE
    elif args.verbose == 1:
        logging_level = logging.WARNING
    elif args.verbose == 2:
        logging_level = logging.NOTICE
    elif args.verbose == 3:
        logging_level = logging.INFO
    
    ch.setLevel(logging_level)
    logger.setLevel(logging_level)

    ch.setFormatter(CustomFormatter())
    logger.addHandler(ch)
    
    def notice(self, message, *args, **kws):
        if self.isEnabledFor(logging.NOTICE):
            self._log(logging.NOTICE, message, args, **kws) 

    def milestone(self, message, *args, **kws):
        if self.isEnabledFor(logging.MILESTONE):
            self._log(logging.MILESTONE, message, args, **kws) 
    
    logging.Logger.notice = notice
    logging.Logger.milestone = milestone
    
    g = Grader(args, logger)
    g.batch_grade()
    g.plagiarism_test()
    g.visualize_plagiarism()
    g.pack_result()
    logger.milestone(f"脚本已执行完成。")
    

if __name__ == "__main__":
    main()
