# 2023年xv6实验评测

由于该年度实验采用git diff生成patch提交，所以测试的流程上需要有些更改，但改动不大，可以只更改config.json实现

## 1. 准备测试文件

同学们的zip需要以`id_name_file.zip`的命名格式放在`student_files`文件夹下面

## 2. 编写config.json

在`config_template_23.json`当中给出了util实验的测试配置模版，需要注意的是，由于本次实现提交的是commit.patch但是有些同学会重命名它，所以我们只匹配.patch的尾缀：

```
"file_list": [
    {
        "src": "^.+\\.patch$",
        "dst": "commit.patch"
    }
]
```

其次，moss_config不能删，删了在初始化的时候过不了，不想改原来的脚本，所以暂时通过命令跳过后续的moss查重

在test_env里面的test_script中需要有`git apply commit.patch`命令，不然无法恢复同学们的更改；同时需要根据实验更改测试脚本，替换`./grade-lab-util`即可

result_regex配置需要根据每次实验的满分更改

branch需要根据实验更改

## 3. 测试

这里给出测试脚本`grade.sh`：
```
python grade.py config.json --skip-clone --skip-moss --skip-moss-vis -vv
```
实际上就是测试命令，这里跳过了查重以及仓库的clone，第一次测试的话需要去掉`--skip-clone`

## 4. 注意事项

如果对最后的成绩记录的顺序有要求，请在template.csv当中 **每行** 以 **id,name** 的形式给出