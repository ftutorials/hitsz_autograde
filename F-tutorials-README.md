# 适配到F-tutorials的方法

### 1. 标准化：替换所有学生提交的文件系统名称

运行`format.sh`脚本，将各个学生对应子目录下的代码进行格式化，并移到`student_files`目录下用于测试。
**要求学生代码以zip形式打包，命名为`ID_Name.zip`并放在`student_files_raw`目录下**
```shell
# 执行代码
$ bash ./format.sh
```

```shell
# 格式化结果：
- student_1
    - fs.h
    - fs.c
    - types.h
    - fs.layout
- student_2
    - fs.h
    - fs.c
    - types.h
    - fs.layout
- ......
```

### 2. 编写配置文件

#### 2.1. 根据测试环境编写`config.json`文件

- 配置文件规范参考`hitsz_autograde/Readme.md`
- 提供一个适合本实验的配置文件样例`hitsz_autograde/config_template.json`以供参考

**配置文件编写注意事项**

- 需要根据测试服务器路径信息，创建一个新的branch，并将`config.json`中的`"repository"`和`"branch"`字段更换为对应的值。
- 

#### 2.2. 根据测试测试学生名单编写`template.csv`文件

格式如下：
```
190111111,aaa
190112222,aaa2
190115231,bbb
```

### 3. 运行grade.py脚本进行打分及检测

```shell
# 执行代码
$ python3 ./grade.py config.json
```