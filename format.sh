#!/bin/bash

typesh="types.h"

rm -rf student_files_unzip
mkdir student_files_unzip

# phase 1: unzip from "student_files_raw"
cd ./student_files_raw
for STU_RAW_ZIP in ./* ;
    do
        unzip -n $STU_RAW_ZIP -d ../student_files_unzip
    done
cd ..


cd ./student_files_unzip
for STU in ./* ;
    do
        # phase 2: format
        cd ./$STU
        for file_c in *.c ; do mv "$file_c" "testfs.c" ; done
        for file_layout in *.layout ; do mv "$file_layout" "testfs.layout" ; done
        # for file_h in *.h ; 
        #     do
        #     if [ $file_h != $typesh ] ; then
        #         mv "$file_h" "testfs.h"
        #     fi
        #     done
        cd ../

        # phase 3: zip to student_files
        zip -r ../student_files/$STU.zip ./$STU
    done

cd ../

rm -rf student_files_unzip